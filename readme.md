# Product-Consumer

### Architecture

- `Product` responsiple for holding the data we need to process/store.

- DataSources
    - `ApiDataSource` impelements `DataSource`, responsiple for reading data from external apis.
    - `FileDataSource` impelements `DataSource`, responsiple for reading data from a local file.

    More data sources can be defined by implementing the `DataSource` interface.

- Parsers
    - `JsonParser`, `CsvParsre` and `XmlParser` extend `Parser` abstract class and each responsiple for parsing a data format.
    - `Parser` has two optional arguments, `mapping` can be used to remap the parsed data and `postProcessor` can be used for any post parsing processing like date formatting and currency conversions. an example can be found at the [DataProviderExtra](/app/DataProviders/DataProviderExtra.php).

    More data parsers can be defined by extending the `Parser` class.

- DataProviders
    - `DataProvider` follows the [Factory Method Pattern](https://en.wikipedia.org/wiki/Factory_method_pattern)  to delegate the instantiation logic of `Parser` and `DataSource` needed to child classes.

    More data providers can be defined by extending the `DataProvider` class.





