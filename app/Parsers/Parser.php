<?php

namespace App\Parsers;

abstract class Parser
{
    /**
     * @var string
     */
    private $data;
    /**
     * @var array
     */
    private $parsedData;
    /**
     * @var array
     */
    private $mapping;
    /**
     * @var callable
     */
    private $postProcesser;

    abstract protected function parseData();


    public function __construct(string $data, callable $postProcesser = null, array $mapping = null)
    {
        $this->data = $data;
        $this->postProcesser = $postProcesser;
        $this->mapping = $mapping;
    }

    /**
     * @param string data
     */
    public function setData(string $data)
    {
        $this->data = $data;
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        $valid = $this->parse();
        return $valid;
    }

    /**
     * @return array|false
     */
    public function getParsedData() 
    {
        if ($this->isValid() === false) {
            return false;
        }
        return $this->parsedData;
    }

    /**
     * @return bool
     */
    final private function parse()
    {
        $data = $this->parseData();
        $data = $this->mapData($data);
        $data = $this->postProcess($data);
        if ($data === false) {
            return false;
        }
        $this->parsedData = $data;
        return true;
    }

    /**
     * @return array|false
     */
    final private function mapData($data)
    {
        if (!isset($this->mapping)) {
            return data;
        }
        foreach ($data as $row) {
            foreach ($this->mapping as $originalKey => $newKey) {
                $row->$newKey = $row->$originalKey;
                unset($row->$originalKey);
            }
        }
        return data;
    }

    /**
     * @return array|false
     */
    final private function postProcess($data)
    {
        if (!isset($this->postProcesser)) {
            return data;
        }
        $newData = [];
        foreach ($data as $row) {
            $newData[] = $this->postProcesser($row);
        }
        return $newData;
    }
}