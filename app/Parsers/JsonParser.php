<?php

namespace App\Parsers;

class JsonParser extends Parser
{

    /**
     * @return array|false
     */
    protected function parseData()
    {
        $data = json_decode($this->data);
        if (!isset($data)) {
            return false;
        }
        return $data;
    }
}