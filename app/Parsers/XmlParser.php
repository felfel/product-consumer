<?php

namespace App\Parsers;

class XmlParser extends Parser
{

    /**
     * @return array|false
     */
    protected function parseData()
    {
        $data = simplexml_load_string($this->data);
        return $data;
    }
}