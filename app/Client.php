<?php

namespace App;

use App\DataProviders\{DataProivderX, DataProivderY, DataProivderZ, DataProivderL, DataProivderExtra};

class Client
{
    protected static function getProviders()
    {
    	return [
    		new DataProivderX(),
    		new DataProivderY(),
    		new DataProivderZ(),
    		new DataProivderL(),
    		new DataProivderExtra()
    	];
    }

    public static function saveProducts()
    {
    	$providers = self::getProviders();
    	foreach ($providers as $provider) {
    		$products = $provider->getProducts;
    		if ($products === false) {
    			continue;
    		}
	    	foreach ($products as $product) {
	    		$product->saveToDatabase();
	    	}
    	}

    }
}
