<?php

namespace App\DataProviders;

use App\DataSources\{DataSource, ApiDataSource};
use App\Parsers\{Parser, XmlParser};

class DataProviderY extends DataProvider
{
    protected function makeParser($data): Parser
    {
    	return new XmlParser($data);
    }

    protected function makeDataSource(): DataSource
    {
    	return new ApiDataSource("www.cool.com", "GET", "user:password");
    }
}
