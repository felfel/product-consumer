<?php

namespace App\DataProviders;

use App\Product;
use App\Parsers\Parser;
use App\DataSources\DataSource;

abstract class DataProvider
{
    abstract protected function makeParser($data): Parser;
    abstract protected function makeDataSource(): DataSource;


    /**
     * @return array|false
     */
    public function getProducts()
    {
    	$dataSource = $this->makeDataSource();
    	$data = $dataSource->getData();
    	$parser = $this->makeParser($data);
    	$parsedData = $parser->parse();
        $products = [];
        foreach ($parsedData as $row) {
            $products[] = Product::createFromObject($row);
        }
    	return $products;
    }
}
