<?php

namespace App\DataProviders;

use App\DataSources\{DataSource, FilesDataSource};
use App\Parsers\{Parser, CsvParser};

class DataProviderZ extends DataProvider
{
    protected function makeParser($data): Parser
    {
    	return new CsvParser($data);
    }

    protected function makeDataSource(): DataSource
    {
    	return new FilesDataSource("/somewhere/on/this/machine");
    }
}
