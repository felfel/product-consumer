<?php

namespace App\DataProviders;

use App\DataSources\{DataSource, ApiDataSource};
use App\Parsers\{Parser, JsonParser};

class DataProviderExtra extends DataProvider
{
	const mapping = [
		"title"=>"name",
		"info"=>"description"
	];

	public static function postProcessor (&$row) {
		$row->price = (int) ($row->price * 100);
    }

    protected function makeParser($data): Parser
    {
    	return new JsonParser($data, self::postProcessor, self::mapping);
    }

    protected function makeDataSource(): DataSource
    {
    	$api = new ApiDataSource("www.example.com", "POST", "user:password");
    	$api->setContent('{"filter": "last-24-hours"}');
    	$api->setContentType('application/json');
    	return $api;
    }
}
