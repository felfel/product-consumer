<?php

namespace App\DataProviders;

use App\DataSources\{DataSource, ApiDataSource};
use App\Parsers\{Parser, JsonParser};

class DataProviderX extends DataProvider
{
    protected function makeParser($data): Parser
    {
    	return new JsonParser($data);
    }

    protected function makeDataSource(): DataSource
    {
    	return new ApiDataSource("www.example.com", "GET", "user:password");
    }
}
