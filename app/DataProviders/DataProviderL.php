<?php

namespace App\DataProviders;

use App\DataSources\{DataSource, FilesDataSource};
use App\Parsers\{Parser, JsonParser};

class DataProviderL extends DataProvider
{
    protected function makeParser($data): Parser
    {
    	return new JsonParser($data);
    }

    protected function makeDataSource(): DataSource
    {
    	return new FilesDataSource("/somewhere/on/this/machine");
    }
}
