<?php

namespace App\DataSources;

interface DataSource
{
    /**
     * @return string|false
     */
    public function getData();
}
