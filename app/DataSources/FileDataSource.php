<?php

namespace App\DataSources;

class FileDataSource implements DataSource
{
    /**
     * @var string
     */
    private $path;

    public function __construct($path)
    {
        $this->path = $path;
    }

    /**
     * @return string|false
     */
    public function getData()
    {   
        if(!file_exists($this->path)) {
            return false;
        }
        return file_get_contents($this->path);
    }
}