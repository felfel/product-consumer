<?php

namespace App\DataSources;

class ApiDataSource implements DataSource
{
    /**
     * @var string
     */
    private $url;
    /**
     * @var string
     */
    private $method;
    /**
     * @var string
     */
    private $credentials;
    /**
     * @var string
     */
    private $contentType;
    /**
     * @var string
     */
    private $content;

    public function __construct($url, $method, $credentials)
    {
        $this->url = urlencode($url);
        $this->method = $method;
        $this->credentials = $credentials;
    }

    /**
     * @param string contentType
     */
    public function setContentType($contentType)
    {
        $this->contentType = $contentType;
    }

    /**
     * @param string content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return string|false
     */
    private function getContext()
    {
        $opts = ['http' =>
            [
              'method'  => $this->method,
              'header'  => "Authorization: Basic ".base64_encode($this->credentials)."\r\n",
              'timeout' => 30
            ]
        ];
        if (isset($this->contentType)) {
            $header = "Content-Type: ".$this->contentType."\r\n";
            $opts['header'] = $opts['header'].$header;
        }
        if (isset($this->content)) {
            $opts['content'] = $this->content;
        }
        return stream_context_create($opts);
    }

    /**
     * @return string|false
     */
    public function getData()
    {   
        $context = $this->getContext();
        return file_get_contents($this->url, false, $context);
    }
}