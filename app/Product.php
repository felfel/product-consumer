<?php

namespace App;

class Product
{
    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $description;
    /**
     * @var int
     */
    private $price;

    /**
     * @param object data
     * @return Product
     */
    static public function createFromObject(object $data): Product
    {
        return new self(
            $data->id,
            $data->name,
            $data->description,
            $data->price
        );
    }

    public function __construct(string $id = null, string $name = null, string $description = null, int $price = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->price = $price;
    }


    public function saveToDatabase()
    {
        // product specific database logic goes here;
    }

    /**
     * @param string id
     */
    public function setId(string $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }
    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param int price
     */
    public function setPrice(int $price)
    {
        $this->price = $price;
    }
    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }
}